var bCrypt = require('bcrypt-nodejs');
var exports = module.exports = {};
var models = require('../models');
var Consumer = models.consumer;

exports.addConsumer = function (req,res) {
    var generateHash = function(password) {
        return bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);
    };
    var username = req.user.email;
    var userId = req.user.id;
    var consumerEmail = req.body.email;
    var consumerName = req.body.name;
    var rawConsumerPassword = req.body.password;
    var consumerPassword = generateHash(rawConsumerPassword);
    console.log('started consumer add for '+username+' with title: '+consumerName);
    Consumer.findOrCreate({
        where:{
            name: consumerEmail,
            owner: userId,
            active:1
        },
        defaults:{
            name: consumerName,
            username:consumerEmail,
            password:consumerPassword,
            owner: userId,
            active: 1
        }
    }).spread(function (consumer, created) {
        var consumerID = consumer.get('id');
        if(created){
            console.log('consumer added: '+consumerName);
            res.send(JSON.stringify({
                status:200,
                message:'consumer added with id: '+consumerID}));
        }else {
            console.log('consumer not added');
            res.send(JSON.stringify({status:400,message:'consumer exists with id: '+consumerID}));
        }
    }).catch(function (err) {
        console.log(err);
    });
};

exports.getUserConsumers = function (req,res) {
    var userID = req.user.id;
    Consumer.findAll({
        where:{
            owner:userID,
            active:1
        },attributes:['username','name','id','createdAt'],
        raw:true
    }).then(function (result) {
        res.send({
            status:200,
            consumers:result
        });
    });

};

exports.deleteConsumer = function (req,res) {
    var consumerID = req.params.id;
    var userID = req.user.id;
    console.log('consumer: '+ consumerID+' user: '+userID);
    Consumer.update({
            active:0
        },{
            where:{
                id:consumerID,
                owner:userID,
                active:1
            }}

    ).then(function () {
        res.send({
            status:200,
            message:'deleted consumer '+consumerID
        });
    }).catch(function () {
        res.send({
            status:400,
            message:'cannot delete consumer '+consumerID
        });
    });
};

exports.restoreConsumer = function (req, res) {
    var consumerID = req.params.id;
    var userID = req.user.id;
    console.log('group: '+ consumerID+' user: '+userID);
    Consumer.update({
            active:1
        },{
            where:{
                id:consumerID,
                owner:userID,
                active:0
            }}

    ).then(function () {
        res.send({
            status:200,
            message:'restored consumer '+consumerID
        });
    }).catch(function () {
        res.send({
            status:400,
            message:'cannot restore consumer '+consumerID
        });
    });
};

exports.getGroupsOfConsumer = function (req, res) {

};