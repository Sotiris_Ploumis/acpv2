var exports = module.exports = {};
var models = require('../models');
var PolicyGroup = models.policygroup;
var GroupConsumer = models.groupconsumer;

//policy-groups
exports.getPolicyGroups = function (req, res) {
    var policyID = req.params.id;
    var userID = req.user.id;
    console.log('getting groups for policy: '+ policyID+' user: '+userID);

    models.sequelize.query(
        'SELECT groups.id, groups.name FROM groups WHERE groups.owner = ? AND groups.active = ? ' +
        'AND NOT EXISTS (SELECT NULL FROM policygroups WHERE policygroups.active = ? AND ' +
        'policygroups.owner = groups.owner AND policygroups.groupId = groups.id AND policygroups.policyId = ?)',
        {replacements:[userID,1,1,policyID], type: models.sequelize.QueryTypes.SELECT}
    ).then(function (notConnected) {
        models.sequelize.query(
            'SELECT groups.id, groups.name FROM groups, policygroups WHERE groups.active = ? AND ' +
            'groups.id = policygroups.groupId AND policygroups.active = ? AND policygroups.policyId = ?',
            {replacements:[1,1,policyID], type: models.sequelize.QueryTypes.SELECT}
        ).then(function (connected) {
            res.send({
                status:200,
                groups:{'notconnected':notConnected,'connected':connected}
            });
            return null;
        }).catch(function () {
            res.send({
                status:400,
                message:'cannot retrieve groups for this policy'
            });
            return null;
        });
        return null;
    }).catch(function () {
        res.send({
            status:400,
            message:'cannot retrieve groups for this policy'
        });
        return null;
    });
};
exports.getGroupPolicies = function (req, res) {
    var groupID = req.params.id;
    var userID = req.user.id;
    console.log('getting policies for group: '+ groupID+' user: '+userID);

    models.sequelize.query(
        'SELECT policies.id, policies.name FROM policies WHERE policies.owner = ? AND policies.active = ? ' +
        'AND NOT EXISTS (SELECT NULL FROM policygroups WHERE policygroups.active = ? AND ' +
        'policygroups.owner = policies.owner AND policygroups.policyId = policies.id AND policygroups.groupId = ?)',
        {replacements:[userID,1,1,groupID], type: models.sequelize.QueryTypes.SELECT}
    ).then(function (notConnected) {
        models.sequelize.query(
            'SELECT policies.id, policies.name FROM policies, policygroups WHERE policies.active = ? AND ' +
            'policies.id = policygroups.policyId AND policygroups.active = ? AND policygroups.groupId = ?',
            {replacements:[1,1,groupID], type: models.sequelize.QueryTypes.SELECT}
        ).then(function (connected) {
            res.send({
                status:200,
                policies:{'notconnected':notConnected,'connected':connected}
            });
            return null;
        }).catch(function () {
            res.send({
                status:400,
                message:'cannot retrieve policies for this group'
            });
            return null;
        });
        return null;
    }).catch(function () {
        res.send({
            status:400,
            message:'cannot retrieve policies for this group'
        });
        return null;
    });
};
exports.addPolicyGroup = function (req, res) {
    var userID = req.user.id;
    var policyID = req.params.pid;
    var groupID = req.params.gid;
    PolicyGroup.findOrCreate({
        where:{
            owner:userID,
            active:1,
            policyId:policyID,
            groupId:groupID
        },
        defaults:{
            owner:userID,
            active:1,
            policyId:policyID,
            groupId:groupID
        }
    }).spread(function () {
        res.send({
            status:200,
            message:'added pg relation'
        });
    }).catch(function (err) {
        res.send({
            status:400,
            message:err.message
        });
    })
};
exports.deletePolicyGroup = function (req, res) {
    var userID = req.user.id;
    var policyID = req.params.pid;
    var groupID = req.params.gid;
    PolicyGroup.update({
            active:0
        },{
            where:{
                policyId:policyID,
                groupId:groupID,
                owner:userID,
                active:1
            }}

    ).then(function () {
        res.send({
            status:200,
            message:'removed pg relation'
        });
    }).catch(function (err) {
        res.send({
            status:400,
            message:err.message
        });
    });
};

//groups-consumers
exports.getGroupConsumers = function (req, res) {
    var groupID = req.params.id;
    var userID = req.user.id;
    console.log('getting consumers for group: '+ groupID+' user: '+userID);

    models.sequelize.query(
        'SELECT consumers.id, consumers.name FROM consumers WHERE consumers.owner = ? AND consumers.active = ? ' +
        'AND NOT EXISTS (SELECT NULL FROM groupconsumers WHERE groupconsumers.active = ? AND ' +
        'groupconsumers.owner = consumers.owner AND groupconsumers.consumerId = consumers.id AND groupconsumers.groupId = ?)',
        {replacements:[userID,1,1,groupID], type: models.sequelize.QueryTypes.SELECT}
    ).then(function (notConnected) {
        models.sequelize.query(
            'SELECT consumers.id, consumers.name FROM consumers, groupconsumers WHERE consumers.active = ? AND ' +
            'consumers.id = groupconsumers.consumerId AND groupconsumers.active = ? AND groupconsumers.groupId = ?',
            {replacements:[1,1,groupID], type: models.sequelize.QueryTypes.SELECT}
        ).then(function (connected) {
            res.send({
                status:200,
                groups:{'notconnected':notConnected,'connected':connected}
            });
            return null;
        }).catch(function () {
            res.send({
                status:400,
                message:'cannot retrieve consumers for this group'
            });
            return null;
        });
        return null;
    }).catch(function () {
        res.send({
            status:400,
            message:'cannot retrieve consumers for this group'
        });
        return null;
    });
};
exports.getConsumerGroups = function (req, res) {
    var consumerID = req.params.id;
    var userID = req.user.id;
    console.log('getting groups for consumer: '+ consumerID+' user: '+userID);

    models.sequelize.query(
        'SELECT groups.id, groups.name FROM groups WHERE groups.owner = ? AND groups.active = ? ' +
        'AND NOT EXISTS (SELECT NULL FROM groupconsumers WHERE groupconsumers.active = ? AND ' +
        'groupconsumers.owner = groups.owner AND groupconsumers.groupId = groups.id AND groupconsumers.consumerId = ?)',
        {replacements:[userID,1,1,consumerID], type: models.sequelize.QueryTypes.SELECT}
    ).then(function (notConnected) {
        models.sequelize.query(
            'SELECT groups.id, groups.name FROM groups, groupconsumers WHERE groups.active = ? AND ' +
            'groups.id = groupconsumers.groupId AND groupconsumers.active = ? AND groupconsumers.consumerId = ?',
            {replacements:[1,1,consumerID], type: models.sequelize.QueryTypes.SELECT}
        ).then(function (connected) {
            res.send({
                status:200,
                groups:{'notconnected':notConnected,'connected':connected}
            });
            return null;
        }).catch(function () {
            res.send({
                status:400,
                message:'cannot retrieve groups for this consumer'
            });
            return null;
        });
        return null;
    }).catch(function () {
        res.send({
            status:400,
            message:'cannot retrieve groups for this consumer'
        });
        return null;
    });
};
exports.addGroupConsumer = function (req, res) {
    var userID = req.user.id;
    var groupID = req.params.gid;
    var consumerID = req.params.cid;
    GroupConsumer.findOrCreate({
        where:{
            owner:userID,
            active:1,
            consumerId:consumerID,
            groupId:groupID
        },
        defaults:{
            owner:userID,
            active:1,
            consumerId:consumerID,
            groupId:groupID
        }
    }).spread(function () {
        res.send({
            status:200,
            message:'added gc relation'
        });
    }).catch(function (err) {
        res.send({
            status:400,
            message:err.message
        });
    })
};
exports.deleteGroupConsumer  =function (req, res) {
    var userID = req.user.id;
    var consumerID = req.params.cid;
    var groupID = req.params.gid;
    GroupConsumer.update({
            active:0
        },{
            where:{
                consumerId:consumerID,
                groupId:groupID,
                owner:userID,
                active:1
            }}

    ).then(function () {
        res.send({
            status:200,
            message:'removed gc relation'
        });
    }).catch(function (err) {
        res.send({
            status:400,
            message:err.message
        });
    });
};