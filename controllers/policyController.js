var exports = module.exports = {};
var models = require('../models');
var Policy = models.policy;
var Users = models.user;

exports.addPolicy = function (req,res) {//TODO replies
    var username = req.user.email;
    var policyName = req.body.name;
    var userId = req.user.id;
    var policyDescription = req.body.description;
    console.log('started policy add for '+username+' with title: '+policyName);
    Policy.findOrCreate({
        where:{
            name: policyName,
            owner: userId,
            active:1
        },
        defaults:{
            name: policyName,
            owner: userId,
            description: policyDescription,
            active: 1,
            uri: 'todo'
        }
    }).spread(function (policy, created) {
        var policyID = policy.get('id');
        var uri = 'check/' + policyID;
        if(created){
            policy.update({uri:uri}).then(function () {
                console.log('policy added: '+policyName);
                res.send(JSON.stringify({
                    status:200,
                    message:'policy added with id: '+policyID}));
            });
            return null;
        }else {
            console.log('policy not added');
            res.send(JSON.stringify({status:400,message:'policy exists with id: '+policyID}));
        }
    }).catch(function (err) {
        console.log(err);
    });
};

exports.getUserPolicies = function (req,res) {
    var userID = req.user.id;
    Policy.findAll({
        where:{
            owner:userID,
            active:1
        },attributes:['name','description','uri','id','createdAt'],
        raw:true
    }).then(function (result) {
            res.send({
                status:200,
                policies:result
            });
    });
};

exports.deletePolicy = function (req,res) {
    var policyID = req.params.id;
    var userID = req.user.id;
    console.log('policy: '+ policyID+' user: '+userID);
    Policy.update({
        active:0
        },{
        where:{
            id:policyID,
            owner:userID,
            active:1
        }}

    ).then(function () {
        res.send({
            status:200,
            message:'deleted policy '+policyID
        });
    }).catch(function () {
        res.send({
            status:400,
            message:'cannot delete policy '+policyID
        });
    });
};

exports.restorePolicy = function (req, res) {
    var policyID = req.params.id;
    var userID = req.user.id;
    console.log('policy: '+ policyID+' user: '+userID);
    Policy.update({
            active:1
        },{
            where:{
                id:policyID,
                owner:userID,
                active:0
            }}

    ).then(function () {
        res.send({
            status:200,
            message:'restored policy '+policyID
        });
    }).catch(function () {
        res.send({
            status:400,
            message:'cannot restore policy '+policyID
        });
    });
};

exports. getGroupsOfPolicy = function (req, res) {

};