var exports = module.exports = {};

exports.signup = function(req, res) {
    console.log('get signup');
    res.render('signup');
};

exports.signin = function(req, res) {
    console.log('get signin');
    res.render('signin');
};

exports.logout = function (req, res) {
    console.log('get logout');
    req.session.destroy(function (err) {
        res.redirect('/');
    });
};

exports.dashboard = function (req, res) {
    console.log('get dashboard');
    res.render('dashboard',{
        firstname : req.user.firstname,
        lastname : req.user.lastname,
        username : req.user.username,
        email : req.user.email,
        lastlogin : req.user.last_login,
        createdat : req.user.createdAt
    });
};
