var bCrypt = require('bcrypt-nodejs');
var exports = module.exports = {};
var models = require('../models');
var Consumer = models.consumer;
var Policy = models.policy;

exports.checkAccess = function (req, res) {
    var isValidPassword = function(userpass, password) {
        return bCrypt.compareSync(password, userpass);
    };
    var signToken = function (token) {
        return token*2;
    };
    var getUserID = function(policyID,callback) {
        Policy.findById(policyID).then(function (policy) {
            if(policy){
                console.log('owner of this policy is: ' + policy.owner);
                return callback(policy.owner);
            }else{
                console.log('owner of this policy is: no one');
                return null;
            }
        });
    };
    var getConsumerID = function (consumerEmail, userID, callback) {
        Consumer.findOne({
            where: {
                username: consumerEmail,
                owner: userID
            }
        }).then(function(consumer) {
            if (!consumer) {

                console.log('no consumer with this email');
                res.send({
                    status:403,
                    message:'Email does not exist'
                });
                return null;
            }
            if (!isValidPassword(consumer.password, consumerPassword)) {
                console.log('Incorrect password for consumer');
                res.send({
                    status:403,
                    message:'Incorrect password'
                });
                return null;
            }
            return callback(consumer.id);
        }).catch(function(err) {
            console.log("Error:", err.message);
            res.send({
                status:404,
                message:err.message
            });
            return null;
        });
    };
    var PGText = 'SELECT groups.id FROM groups, policygroups WHERE groups.active = ? AND ' +
        'groups.id = policygroups.groupId AND policygroups.active = ? AND policygroups.policyId = ?';

    var GCText = 'SELECT groups.id FROM groups, groupconsumers WHERE groups.active = ? AND ' +
        'groups.id = groupconsumers.groupId AND groupconsumers.active = ? AND groupconsumers.consumerId = ?';

    var finalText = GCText+' AND groups.id IN ('+PGText+')';

    var policyID = req.params.pid;
    var token = req.params.rawToken;
    var signedToken = signToken(token);
    var consumerEmail = req.body.email;
    var consumerPassword = req.body.password;
    getUserID(policyID,function (userID) {
        console.log('Owner of this policy is: ' + userID);
        getConsumerID(consumerEmail,userID,function (consumerID) {
            if(consumerID !== null){
                models.sequelize.query(
                    finalText,
                    {replacements:[1,1,consumerID,1,1,policyID],
                        type: models.sequelize.QueryTypes.SELECT}
                ).then(function (PCConnections) {
                    if(PCConnections.length === 0){
                        res.send({
                            status:403,
                            message:'You do not have access to that file'
                        });
                        return null;
                    }else {
                        res.send({
                            status:200,
                            signedtoken:signedToken
                        });
                        return null;
                    }
                }).catch(function (err) {
                    res.send({
                        status:404,
                        message:'Something happened: ' + err.message
                    });
                    return null;
                });
            }
            return null;
        })
    });
};

exports.showAccess = function (req, res) {
    res.render('check');
};