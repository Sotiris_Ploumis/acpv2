var exports = module.exports = {};
var models = require('../models');
var Group = models.group;

exports.addGroup = function (req,res) {//TODO replies
    var username = req.user.email;
    var groupName = req.body.name;
    var userId = req.user.id;
    console.log('started group add for '+username+' with title: '+groupName);
    Group.findOrCreate({
        where:{
            name: groupName,
            owner: userId,
            active:1
        },
        defaults:{
            name: groupName,
            owner: userId,
            active: 1
        }
    }).spread(function (group, created) {
        var groupID = group.get('id');
        if(created){
            console.log('group added: '+groupName);
            res.send(JSON.stringify({
                status:200,
                message:'group added with id: '+groupID}));
        }else {
            console.log('group not added');
            res.send(JSON.stringify({status:400,message:'group exists with id: '+groupID}));
        }
    }).catch(function (err) {
        console.log(err);
    });
};

exports.getUserGroups = function (req,res) {
    var userID = req.user.id;
    Group.findAll({
        where:{
            owner:userID,
            active:1
        },attributes:['name','id','createdAt'],
        raw:true
    }).then(function (result) {
        res.send({
            status:200,
            groups:result
        });
    });

};

exports.deleteGroup = function (req,res) {
    var groupID = req.params.id;
    var userID = req.user.id;
    console.log('group: '+ groupID+' user: '+userID);
    Group.update({
            active:0
        },{
            where:{
                id:groupID,
                owner:userID,
                active:1
            }}

    ).then(function () {
        res.send({
            status:200,
            message:'deleted group '+groupID
        });
    }).catch(function () {
        res.send({
            status:400,
            message:'cannot delete group '+groupID
        });
    });
};

exports.restoreGroup = function (req, res) {
    var groupID = req.params.id;
    var userID = req.user.id;
    console.log('group: '+ groupID+' user: '+userID);
    Group.update({
            active:1
        },{
            where:{
                id:groupID,
                owner:userID,
                active:0
            }}

    ).then(function () {
        res.send({
            status:200,
            message:'restored group '+groupID
        });
    }).catch(function () {
        res.send({
            status:400,
            message:'cannot restore group '+groupID
        });
    });
};

exports.getConsumersOfGroup = function (req, res) {

};

exports.getPoliciesOfGroup = function (req, res) {

};