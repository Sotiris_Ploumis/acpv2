var relationController = require('../controllers/relationController.js');

module.exports = function (app) {
    //policies-groups
    app.get('/policygroup/:id', isLoggedIn, relationController.getPolicyGroups);
    app.get('/grouppolicy/:id', isLoggedIn, relationController.getGroupPolicies);
    app.put('/policygroup/:pid/:gid', isLoggedIn, relationController.addPolicyGroup);
    app.delete('/policygroup/:pid/:gid', isLoggedIn, relationController.deletePolicyGroup);

    //
    app.get('/groupconsumer/:id', isLoggedIn, relationController.getGroupConsumers);
    app.get('/consumergroup/:id', isLoggedIn, relationController.getConsumerGroups);
    app.put('/groupconsumer/:gid/:cid', isLoggedIn, relationController.addGroupConsumer);
    app.delete('/groupconsumer/:gid/:cid', isLoggedIn, relationController.deleteGroupConsumer);

    function isLoggedIn(req,res,next) {
        if(req.isAuthenticated()){
            return next();
        }
        res.status(401).send();//TODO
    }
};
