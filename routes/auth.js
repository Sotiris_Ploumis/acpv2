var authController = require('../controllers/authcontroller.js');
var checkController = require('../controllers/checkController');

module.exports = function(app, passport) {

    //user
    app.get('/signup', authController.signup);
    app.get('/signin', authController.signin);

    app.post('/signup', passport.authenticate('local-signup', {
            successRedirect: '/',
            failureRedirect: '/signup'
        }
    ));
    app.post('/signin', passport.authenticate('local-signin', {
            successRedirect: '/',
            failureRedirect: '/signin'
        }
    ));
    app.get('/logout', authController.logout);
    app.get('/dashboard',isLoggedIn, authController.dashboard);

    //consumer check
    app.post('/check/:pid/:rawToken',checkController.checkAccess);
    app.get('/check/:pid/:rawToken', checkController.showAccess);

    function isLoggedIn(req,res,next) {
        if(req.isAuthenticated()){
            return next();
        }
        res.redirect('/signin');
    }
};