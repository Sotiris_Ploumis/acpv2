var consumerController = require('../controllers/consumerController.js');

module.exports = function (app) {
    app.put('/consumer', isLoggedIn, consumerController.addConsumer);
    app.get('/consumer', isLoggedIn, consumerController.getUserConsumers);
    app.delete('/consumer/:id', isLoggedIn, consumerController.deleteConsumer);
    app.put('/consumer/:id', isLoggedIn, consumerController.restoreConsumer);

    function isLoggedIn(req,res,next) {
        if(req.isAuthenticated()){
            return next();
        }
        res.status(401).send();//TODO
    }
};
