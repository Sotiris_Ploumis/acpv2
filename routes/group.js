var groupController = require('../controllers/groupController.js');

module.exports = function (app) {
    app.put('/group', isLoggedIn, groupController.addGroup);
    app.get('/group', isLoggedIn, groupController.getUserGroups);
    app.delete('/group/:id', isLoggedIn, groupController.deleteGroup);
    app.put('/group/:id', isLoggedIn, groupController.restoreGroup);

    function isLoggedIn(req,res,next) {
        if(req.isAuthenticated()){
            return next();
        }
        res.status(401).send();//TODO
    }
};
