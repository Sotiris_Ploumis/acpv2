var policyController = require('../controllers/policyController.js');

module.exports = function (app) {
    app.put('/policy', isLoggedIn, policyController.addPolicy);
    app.get('/policy', isLoggedIn, policyController.getUserPolicies);
    app.delete('/policy/:id', isLoggedIn, policyController.deletePolicy);
    app.put('/policy/:id', isLoggedIn, policyController.restorePolicy);

    function isLoggedIn(req,res,next) {
        if(req.isAuthenticated()){
            return next();
        }
        res.status(401).send();//TODO
    }
};
