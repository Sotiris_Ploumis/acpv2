var getInfo = function () {
    getPolicies();
    getGroups();
    getConsumers();
};

//policies
var toggleAddPolicy = function () {
    var addPolicy = document.getElementById('addPolicy');
    var addButton = document.getElementById('addPolicyButton');
    if(addPolicy.style.display === ''){
        addPolicy.style.display = 'none';
        addButton.classList.remove('glyphicon-minus');
        addButton.classList.add('glyphicon-plus');
    }else {
        addPolicy.style.display = '';
        addButton.classList.remove('glyphicon-plus');
        addButton.classList.add('glyphicon-minus');
    }
};
var createPolicy = function (event) {
    event.preventDefault();
    var name = $('#policyName').val();
    var description = $('#policyDescription').val();
    console.log('attempting new policy: '+name+' '+description);
    var parameters = {name: name, description: description};
    $.ajax({
        url: '/policy',
        type: 'PUT',
        data: JSON.stringify(parameters),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: true,
        success: function (result) {
            var status = result['status'];
            if(status === 200){
                console.log(result['message']);
                getPolicies();
                document.getElementById('addPolicy').reset();
                toggleAddPolicy();
            }else {
                console.log(result['message']);
                alert(result['message']);
            }
        }
    });
};
var getPolicies = function () {
    var userPolicies = document.getElementById('policies');
    $.ajax({
        url: '/policy',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: true,
        success: function (result) {
            userPolicies.innerHTML = '';
            var status = result['status'];
            if(status === 200){
                var policyList = result['policies'];
                policyList.forEach(function (policy) {
                    var id = policy['id'];
                    var name = policy['name'];
                    var description = policy['description'];
                    var uri = policy['uri'];
                    var created = policy['createdAt'];

                    //policy box
                    var policyDIV = document.createElement('DIV');
                    policyDIV.id = 'p'+id;
                    policyDIV.classList.add('policies','text-left');

                    //delete button
                    var deleteButton = document.createElement('a');
                    deleteButton.classList.add('close');
                    deleteButton.setAttribute('aria-label','close');
                    deleteButton.onclick = function () {
                        deletePolicy(id);
                    };
                    deleteButton.innerHTML = '&times';
                    policyDIV.appendChild(deleteButton);

                    //title
                    var nameElement = document.createElement('h3');
                    nameElement.innerHTML = name;
                    policyDIV.appendChild(nameElement);

                    //description
                    var descriptionElement = document.createElement('p');
                    descriptionElement.innerHTML = description;
                    policyDIV.appendChild(descriptionElement);

                    //uri
                    var uriDiv = document.createElement('div');
                    uriDiv.innerHTML = uri;
                    policyDIV.appendChild(uriDiv);

                    //created
                    var createdDiv = document.createElement('div');
                    createdDiv.innerHTML = created;
                    policyDIV.appendChild(createdDiv);

                    //relation
                    var addPolicyGroupRelation = document.createElement('button');
                    addPolicyGroupRelation.innerHTML = 'Group relations';
                    addPolicyGroupRelation.setAttribute('data-toggle','modal');
                    addPolicyGroupRelation.setAttribute('data-target','#addPolicyToGroupsModal');
                    addPolicyGroupRelation.classList.add('btn','btn-primary');
                    addPolicyGroupRelation.onclick = function () {
                        showPolicyGroupsModal(id,name);
                    };
                    policyDIV.appendChild(addPolicyGroupRelation);

                    userPolicies.appendChild(policyDIV);
                });
            }else {
                console.log(result['message']);
                alert(result['message']);
            }
        }
    });
};
var deletePolicy = function (id) {
    if (confirm("Are you sure you want to delete this policy?") === true){
        var notificationBar = document.getElementById('action');
        var policyBox = document.getElementById('p'+id);
        var actionMessage = document.getElementById('actionMessage');
        var undoButton = document.getElementById('undo');
        $.ajax({
            url: '/policy/'+id,
            type: 'DELETE',
            async: true,
            success: function (result) {
                var status = result['status'];
                if(status === 200){
                    console.log(result['message']);
                    actionMessage.innerHTML = "Policy "+""+" deleted";
                    notificationBar.style.display = '';
                    undoButton.style.display = '';
                    policyBox.style.display = 'none';
                    undoButton.onclick = function () {
                        restorePolicy(id);
                    }
                }else {
                    console.log(result['message']);
                    alert(result['message']);
                }
            }
        });
    }
};
var restorePolicy = function (id) {
    var policyBox = document.getElementById('p'+id);
    var actionMessage = document.getElementById('actionMessage');
    var undoButton = document.getElementById('undo');
    $.ajax({
        url: '/policy/'+id,
        type: 'PUT',
        async: true,
        success: function (result) {
            var status = result['status'];
            if(status === 200){
                console.log(result['message']);
                actionMessage.innerHTML = "Policy "+""+" restored";
                policyBox.style.display = '';
                undoButton.style.display = 'none';
            }else {
                console.log(result['message']);
                alert(result['message']);
            }
        }
    });
};

//groups
var toggleAddGroup = function () {
    var addGroup = document.getElementById('addGroup');
    var addButton = document.getElementById('addGroupButton');
    if(addGroup.style.display === ''){
        addGroup.style.display = 'none';
        addButton.classList.remove('glyphicon-minus');
        addButton.classList.add('glyphicon-plus');
    }else {
        addGroup.style.display = '';
        addButton.classList.remove('glyphicon-plus');
        addButton.classList.add('glyphicon-minus');
    }
};
var createGroup = function (event) {
    event.preventDefault();
    var name = $('#groupName').val();
    console.log('attempting new group: '+name);
    var parameters = {name: name};
    $.ajax({
        url: '/group',
        type: 'PUT',
        data: JSON.stringify(parameters),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: true,
        success: function (result) {
            var status = result['status'];
            if(status === 200){
                console.log(result['message']);
                getGroups();
                document.getElementById('addGroup').reset();
                toggleAddGroup();
            }else {
                console.log(result['message']);
                alert(result['message']);
            }
        }
    });
};
var getGroups = function () {
    var groups = document.getElementById('groups');
    $.ajax({
        url: '/group',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: true,
        success: function (result) {
            groups.innerHTML = '';
            var status = result['status'];
            if(status === 200){
                var groupList = result['groups'];
                groupList.forEach(function (group) {
                    var id = group['id'];
                    var name = group['name'];
                    var created = group['createdAt'];

                    //group box
                    var groupDIV = document.createElement('DIV');
                    groupDIV.id = 'g'+id;
                    groupDIV.classList.add('groups','text-left');

                    //delete button
                    var deleteButton = document.createElement('a');
                    deleteButton.classList.add('close');
                    deleteButton.setAttribute('aria-label','close');
                    deleteButton.onclick = function () {
                        deleteGroup(id);
                    };
                    deleteButton.innerHTML = '&times';
                    groupDIV.appendChild(deleteButton);

                    //title
                    var nameElement = document.createElement('h3');
                    nameElement.innerHTML = name;
                    groupDIV.appendChild(nameElement);

                    //created
                    var createdElement = document.createElement('p');
                    createdElement.innerHTML = created;
                    groupDIV.appendChild(createdElement);

                    //add policy button
                    var addPolicyButton = document.createElement('button');
                    addPolicyButton.innerHTML = 'Policy relations';
                    addPolicyButton.setAttribute('data-toggle','modal');
                    addPolicyButton.setAttribute('data-target','#addGroupToPoliciesModal');
                    addPolicyButton.classList.add('btn','btn-primary');
                    addPolicyButton.onclick = function () {
                        showGroupPoliciesModal(id,name);
                    };
                    groupDIV.appendChild(addPolicyButton);

                    //add person button
                    var addPersonButton = document.createElement('button');
                    addPersonButton.innerHTML = 'Consumer relations';
                    addPersonButton.setAttribute('data-toggle','modal');
                    addPersonButton.setAttribute('data-target','#addGroupToConsumersModal');
                    addPersonButton.classList.add('btn','btn-primary');
                    addPersonButton.onclick = function () {
                        showGroupConsumerModal(id,name);
                    };
                    groupDIV.appendChild(addPersonButton);

                    groups.appendChild(groupDIV);
                });
            }else {
                console.log(result['message']);
                alert(result['message']);
            }
        }
    });
};
var deleteGroup = function (id) {
    if (confirm("Are you sure you want to delete this group?") === true){
        var notificationBar = document.getElementById('action');
        var actionMessage = document.getElementById('actionMessage');
        var undoButton = document.getElementById('undo');
        $.ajax({
            url: '/group/'+id,
            type: 'DELETE',
            async: true,
            success: function (result) {
                var status = result['status'];
                if(status === 200){
                    undoButton.style.display = '';
                    actionMessage.innerHTML = "Group deleted";
                    notificationBar.style.display = '';
                    undoButton.onclick = function () {
                        restoreGroup(id);
                    };
                    document.getElementById("g"+id).style.display = 'none';
                }else {
                    console.log(result['message']);
                    alert(result['message']);
                }
            }
        });
    }
};
var restoreGroup = function (id) {
    var notificationBar = document.getElementById('action');
    var actionMessage = document.getElementById('actionMessage');
    var undoButton = document.getElementById('undo');
    $.ajax({
        url: '/group/'+id,
        type: 'PUT',
        async: true,
        success: function (result) {
            var status = result['status'];
            if(status === 200){
                undoButton.style.display = 'none';
                actionMessage.innerHTML = "Group restored";
                notificationBar.style.display = '';
                document.getElementById('g'+id).style.display = '';
            }else {
                console.log(result['message']);
                alert(result['message']);
            }
        }
    });
};

//consumers
var toggleAddConsumer = function () {
    var addButton = document.getElementById('addConsumerButton');
    var addConsumer = document.getElementById('addConsumer');
    if(addConsumer.style.display === ''){
        addConsumer.style.display = 'none';
        addButton.classList.remove('glyphicon-minus');
        addButton.classList.add('glyphicon-plus');
    }else {
        addConsumer.style.display = '';
        addButton.classList.remove('glyphicon-plus');
        addButton.classList.add('glyphicon-minus');
    }
};
var createConsumer = function (event) {
    event.preventDefault();
    var name = $('#consumerName').val();
    var email = $('#consumerEmail').val();
    var password = $('#consumerPassword').val();
    console.log('attempting new consumer: '+name);
    var parameters = {
        name:name,
        email:email,
        password:password
    };
    $.ajax({
        url:'/consumer',
        type:'PUT',
        data: JSON.stringify(parameters),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: true,
        success: function (result) {
            var status = result['status'];
            if(status === 200){
                console.log(result['message']);
                getConsumers();
                document.getElementById('addConsumer').reset();
                toggleAddConsumer();
            }else {
                console.log(result['message']);
                alert(result['message']);
            }
        }
    });
};
var getConsumers = function () {
    var consumers = document.getElementById('consumers');
    $.ajax({
        url: '/consumer',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: true,
        success: function (result) {
            consumers.innerHTML = '';
            var status = result['status'];
            if(status === 200){
                var consumerList = result['consumers'];
                consumerList.forEach(function (consumer) {
                    var id = consumer['id'];
                    var name = consumer['name'];
                    var email = consumer['username'];
                    var created = consumer['createdAt'];

                    //consumer box
                    var consumerDIV = document.createElement('DIV');
                    consumerDIV.id = 'c'+id;
                    consumerDIV.classList.add('consumers','text-left');

                    //delete button
                    var deleteButton = document.createElement('a');
                    deleteButton.classList.add('close');
                    deleteButton.setAttribute('aria-label','close');
                    deleteButton.onclick = function () {
                        deleteConsumer(id);
                    };
                    deleteButton.innerHTML = '&times';
                    consumerDIV.appendChild(deleteButton);

                    //name
                    var nameElement = document.createElement('h3');
                    nameElement.innerHTML = name;
                    consumerDIV.appendChild(nameElement);

                    //email
                    var emailElement = document.createElement('p');
                    emailElement.innerHTML = email;
                    consumerDIV.appendChild(emailElement);

                    //created
                    var createdElement = document.createElement('p');
                    createdElement.innerHTML = created;
                    consumerDIV.appendChild(createdElement);

                    //add group button
                    var addGroupButton = document.createElement('button');
                    addGroupButton.innerHTML = 'Group relations';
                    addGroupButton.setAttribute('data-toggle','modal');
                    addGroupButton.setAttribute('data-target','#addConsumerToGroupsModal');
                    addGroupButton.classList.add('btn','btn-primary');
                    addGroupButton.onclick = function () {
                        showConsumerGroupsModal(id,name);
                    };
                    consumerDIV.appendChild(addGroupButton);

                    consumers.appendChild(consumerDIV);
                });
            }else {
                console.log(result['message']);
                alert(result['message']);
            }
        }
    });
};
var deleteConsumer = function (id) {
    if (confirm("Are you sure you want to delete this person?") === true){
        var notificationBar = document.getElementById('action');
        var actionMessage = document.getElementById('actionMessage');
        var undoButton = document.getElementById('undo');
        $.ajax({
            url: '/consumer/'+id,
            type: 'DELETE',
            async: true,
            success: function (result) {
                var status = result['status'];
                if(status === 200){
                    undoButton.style.display = '';
                    actionMessage.innerHTML = "Person deleted";
                    notificationBar.style.display = '';
                    undoButton.onclick = function () {
                        restoreConsumer(id);
                    };
                    document.getElementById('c'+id).style.display = 'none';
                }else {
                    console.log(result['message']);
                    alert(result['message']);
                }
            }
        });
    }
};
var restoreConsumer = function (id) {
    var notificationBar = document.getElementById('action');
    var actionMessage = document.getElementById('actionMessage');
    var undoButton = document.getElementById('undo');
    $.ajax({
        url: '/consumer/'+id,
        type: 'PUT',
        async: true,
        success: function (result) {
            var status = result['status'];
            if(status === 200){
                undoButton.style.display = 'none';
                actionMessage.innerHTML = "Person restored";
                notificationBar.style.display = '';
                document.getElementById('c'+id).style.display = '';
            }else {
                console.log(result['message']);
                alert(result['message']);
            }
        }
    });
};

//policies-groups
var showPolicyGroupsModal = function (policyID,policyName) {
    var modalTitle = document.getElementById('addPGTitle');
    modalTitle.innerHTML = 'Add and remove groups to or from '+policyName;
    var addBody = document.getElementById('addPGBody');
    addBody.innerHTML = '';
    var removeBody = document.getElementById('removePGBody');
    removeBody.innerHTML = '';

    getPolicyGroups(policyID,function (result) {
        if(result['status'] === 200){
            var allGroups = result['groups'];
            var notConnectedGroups = allGroups['notconnected'];
            notConnectedGroups.forEach(function (group) {
                var item = document.createElement('button');
                item.innerHTML = 'Add '+group['name'];
                item.setAttribute('value',group['id']);
                item.setAttribute('id',group['name']);
                item.onclick = function () {
                    addPolicyGroup(this,policyID,group['id'],group['name']);
                };
                addBody.appendChild(item);
            });
            var connectedGroups = allGroups['connected'];
            connectedGroups.forEach(function (group) {
                var item = document.createElement('button');
                item.innerHTML = 'Remove '+group['name'];
                item.setAttribute('value',group['id']);
                item.setAttribute('id',group['name']);
                item.onclick = function () {
                    deletePolicyGroup(this,policyID,group['id'],group['name']);
                };
                removeBody.appendChild(item);
            });
        }else{
            alert(result['message']);
        }
    });
};
var getPolicyGroups = function (policyID, callback) {
    $.ajax({
        type:'GET',
        url:'/policygroup/'+policyID,
        async:true,
        success:function (result) {
            return callback(result);
        }
    });
};
var addPolicyGroup = function (choice,policyID, groupID, groupName) {
    var removeBody = document.getElementById('removePGBody');
    $.ajax({
        type:'PUT',
        url:'/policygroup/'+policyID+'/'+groupID,
        async:true,
        success:function (result) {
            if(result['status'] === 200){
                var copy = choice.cloneNode(true);
                choice.remove();
                copy.innerHTML = 'Remove '+groupName;
                copy.onclick = function () {
                    deletePolicyGroup(this,policyID,groupID,groupName);
                };
                removeBody.appendChild(copy);
                console.log(result['message']);
            }else{
                alert(result['message']);
            }
        }
    });
};
var deletePolicyGroup = function (choice,policyID, groupID, groupName) {
    var addBody = document.getElementById('addPGBody');
    $.ajax({
        type:'DELETE',
        url:'/policygroup/'+policyID+'/'+groupID,
        async:true,
        success:function (result) {
            if(result['status'] === 200){
                var copy = choice.cloneNode(true);
                choice.remove();
                copy.innerHTML = 'Add '+groupName;
                copy.onclick = function () {
                    addPolicyGroup(this,policyID,groupID,groupName);
                };
                addBody.appendChild(copy);
                console.log(result['message']);
            }else{
                alert(result['message']);
            }
        }
    });
};

//groups-policies
var showGroupPoliciesModal = function (groupID, groupName) {
    var modalTitle = document.getElementById('addGPTitle');
    modalTitle.innerHTML = 'Add and remove policies to or from '+groupName;
    var addBody = document.getElementById('addGPBody');
    addBody.innerHTML = '';
    var removeBody = document.getElementById('removeGPBody');
    removeBody.innerHTML = '';

    getGroupPolicies(groupID,function (result) {
        if(result['status'] === 200){
            var allPolicies = result['policies'];
            var notConnectedPolicies = allPolicies['notconnected'];
            notConnectedPolicies.forEach(function (policy) {
                var item = document.createElement('button');
                item.innerHTML = 'Add '+policy['name'];
                item.setAttribute('value',policy['id']);
                item.setAttribute('id',policy['name']);
                item.onclick = function () {
                    addGroupPolicy(this,groupID,policy['id'],policy['name']);
                };
                addBody.appendChild(item);
            });
            var connectedPolicies = allPolicies['connected'];
            connectedPolicies.forEach(function (policy) {
                var item = document.createElement('button');
                item.innerHTML = 'Remove '+policy['name'];
                item.setAttribute('value',policy['id']);
                item.setAttribute('id',policy['name']);
                item.onclick = function () {
                    deleteGroupPolicy(this,groupID,policy['id'],policy['name']);
                };
                removeBody.appendChild(item);
            });
        }else{
            alert(result['message']);
        }
    });
};
var getGroupPolicies = function (groupID, callback) {
    $.ajax({
        type:'GET',
        url:'/grouppolicy/'+groupID,
        async:true,
        success:function (result) {
            return callback(result);
        }
    });
};
var addGroupPolicy = function (choice, groupID, policyID, policyName) {
    var removeBody = document.getElementById('removeGPBody');
    $.ajax({
        type:'PUT',
        url:'/policygroup/'+policyID+'/'+groupID,
        async:true,
        success:function (result) {
            if(result['status'] === 200){
                var copy = choice.cloneNode(true);
                choice.remove();
                copy.innerHTML = 'Remove '+policyName;
                copy.onclick = function () {
                    deleteGroupPolicy(this,groupID,policyID,policyName);
                };
                removeBody.appendChild(copy);
                console.log(result['message']);
            }else{
                alert(result['message']);
            }
        }
    });
};
var deleteGroupPolicy = function (choice, groupID, policyID, policyName) {
    var addBody = document.getElementById('addGPBody');
    $.ajax({
        type:'DELETE',
        url:'/policygroup/'+policyID+'/'+groupID,
        async:true,
        success:function (result) {
            if(result['status'] === 200){
                var copy = choice.cloneNode(true);
                choice.remove();
                copy.innerHTML = 'Add '+policyName;
                copy.onclick = function () {
                    addGroupPolicy(this,groupID,policyID,policyName);
                };
                addBody.appendChild(copy);
                console.log(result['message']);
            }else{
                alert(result['message']);
            }
        }
    });
};

//groups consumers
var showGroupConsumerModal = function (groupID, groupName) {
    var modalTitle = document.getElementById('addGCTitle');
    modalTitle.innerHTML = 'Add and remove consumers to or from '+groupName;
    var addBody = document.getElementById('addGCBody');
    addBody.innerHTML = '';
    var removeBody = document.getElementById('removeGCBody');
    removeBody.innerHTML = '';

    getGroupConsumers(groupID,function (result) {
        if(result['status'] === 200){
            var allConsumers = result['groups'];
            var notConnectedConsumers = allConsumers['notconnected'];
            notConnectedConsumers.forEach(function (consumer) {
                var item = document.createElement('button');
                item.innerHTML = 'Add '+consumer['name'];
                item.setAttribute('value',consumer['id']);
                item.setAttribute('id',consumer['name']);
                item.onclick = function () {
                    addGroupConsumer(this,groupID,consumer['id'],consumer['name']);
                };
                addBody.appendChild(item);
            });
            var connectedConsumers = allConsumers['connected'];
            connectedConsumers.forEach(function (consumers) {
                var item = document.createElement('button');
                item.innerHTML = 'Remove '+consumers['name'];
                item.setAttribute('value',consumers['id']);
                item.setAttribute('id',consumers['name']);
                item.onclick = function () {
                    deleteGroupConsumer(this,groupID,consumers['id'],consumers['name']);
                };
                removeBody.appendChild(item);
            });
        }else{
            alert(result['message']);
        }
    });
};
var getGroupConsumers = function (groupID, callback) {
    $.ajax({
        type:'GET',
        url:'/groupconsumer/'+groupID,
        async:true,
        success:function (result) {
            return callback(result);
        }
    });
};
var addGroupConsumer = function (choice, groupID, consumerID, consumerName) {
    var removeBody = document.getElementById('removeGCBody');
    $.ajax({
        type:'PUT',
        url:'/groupconsumer/'+groupID+'/'+consumerID,
        async:true,
        success:function (result) {
            if(result['status'] === 200){
                var copy = choice.cloneNode(true);
                choice.remove();
                copy.innerHTML = 'Remove '+consumerName;
                copy.onclick = function () {
                    deleteGroupConsumer(this,groupID,consumerID,consumerName);
                };
                removeBody.appendChild(copy);
                console.log(result['message']);
            }else{
                alert(result['message']);
            }
        }
    });
};
var deleteGroupConsumer = function (choice, groupID, consumerID, consumerName) {
    var addBody = document.getElementById('addGCBody');
    $.ajax({
        type:'DELETE',
        url:'/groupconsumer/'+groupID+'/'+consumerID,
        async:true,
        success:function (result) {
            if(result['status'] === 200){
                var copy = choice.cloneNode(true);
                choice.remove();
                copy.innerHTML = 'Add '+consumerName;
                copy.onclick = function () {
                    addGroupConsumer(this,groupID,consumerID,consumerName);
                };
                addBody.appendChild(copy);
                console.log(result['message']);
            }else{
                alert(result['message']);
            }
        }
    });
};

//consumers groups
var showConsumerGroupsModal = function (consumerID, consumerName) {
    var modalTitle = document.getElementById('addCGTitle');
    modalTitle.innerHTML = 'Add and remove groups to or from '+consumerName;
    var addBody = document.getElementById('addCGBody');
    addBody.innerHTML = '';
    var removeBody = document.getElementById('removeCGBody');
    removeBody.innerHTML = '';

    getConsumerGroups(consumerID,function (result) {
        if(result['status'] === 200){
            var allGroups = result['groups'];
            var notConnectedGroups = allGroups['notconnected'];
            notConnectedGroups.forEach(function (group) {
                var item = document.createElement('button');
                item.innerHTML = 'Add '+group['name'];
                item.setAttribute('value',group['id']);
                item.setAttribute('id',group['name']);
                item.onclick = function () {
                    addConsumerGroup(this,consumerID,group['id'],group['name']);
                };
                addBody.appendChild(item);
            });
            var connectedGroups = allGroups['connected'];
            connectedGroups.forEach(function (group) {
                var item = document.createElement('button');
                item.innerHTML = 'Remove '+group['name'];
                item.setAttribute('value',group['id']);
                item.setAttribute('id',group['name']);
                item.onclick = function () {
                    deleteConsumerGroup(this,consumerID,group['id'],group['name']);
                };
                removeBody.appendChild(item);
            });
        }else{
            alert(result['message']);
        }
    });
};
var getConsumerGroups = function (consumerID, callback) {
    $.ajax({
        type:'GET',
        url:'/consumergroup/'+consumerID,
        async:true,
        success:function (result) {
            return callback(result);
        }
    });
};
var addConsumerGroup = function (choice, consumerID, groupID, groupName) {
    var removeBody = document.getElementById('removeCGBody');
    $.ajax({
        type:'PUT',
        url:'/groupconsumer/'+groupID+'/'+consumerID,
        async:true,
        success:function (result) {
            if(result['status'] === 200){
                var copy = choice.cloneNode(true);
                choice.remove();
                copy.innerHTML = 'Remove '+groupName;
                copy.onclick = function () {
                    deleteConsumerGroup(this,groupID,consumerID,groupName);
                };
                removeBody.appendChild(copy);
                console.log(result['message']);
            }else{
                alert(result['message']);
            }
        }
    });
};
var deleteConsumerGroup = function (choice, consumerID, groupID, groupName) {
    var addBody = document.getElementById('addCGBody');
    $.ajax({
        type:'DELETE',
        url:'/groupconsumer/'+groupID+'/'+consumerID,
        async:true,
        success:function (result) {
            if(result['status'] === 200){
                var copy = choice.cloneNode(true);
                choice.remove();
                copy.innerHTML = 'Add '+groupName;
                copy.onclick = function () {
                    addConsumerGroup(this,groupID,consumerID,groupName);
                };
                addBody.appendChild(copy);
                console.log(result['message']);
            }else{
                alert(result['message']);
            }
        }
    });
};

//other
var closeNotification = function () {
    document.getElementById('action').style.display = 'none';
};