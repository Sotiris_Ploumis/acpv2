"use strict";

var fs = require("fs");
var path = require("path");
var Sequelize = require("sequelize");
var env = process.env.NODE_ENV || "development";
var config = require(path.join(__dirname, '..', 'config', 'config.json'))[env];
var sequelize = new Sequelize(config.database, config.username, config.password, config);
var db = {};


fs
    .readdirSync(__dirname)
    .filter(function(file) {
        return (file.indexOf(".") !== 0) && (file !== "index.js");
    })
    .forEach(function(file) {
        var model = sequelize.import(path.join(__dirname, file));
        console.log('model: ' + model.name);
        db[model.name] = model;
    });

Object.keys(db).forEach(function(modelName) {
    if ("associate" in db[modelName]) {
        db[modelName].associate(db);
    }
});



db.sequelize = sequelize;
db.Sequelize = Sequelize;

//associations
db['user'].hasMany(db['policy'],{foreignKey:'owner', sourceKey:'id'});
db['user'].hasMany(db['group'],{foreignKey:'owner', sourceKey:'id'});
db['user'].hasMany(db['consumer'],{foreignKey:'owner', sourceKey:'id'});
db['user'].hasMany(db['policygroup'],{foreignKey:'owner', sourceKey:'id'});
db['user'].hasMany(db['groupconsumer'],{foreignKey:'owner', sourceKey:'id'});

//lines in comment not working because sequelize does not support
//duplicate entries by foreign keys in joint tables M-M
//db['policy'].belongsToMany(db['group'],{through:db['policygroup']});
//db['group'].belongsToMany(db['policy'],{through:db['policygroup']});

//db['group'].belongsToMany(db['consumer'],{through:db['groupconsumer']});
//db['consumer'].belongsToMany(db['group'],{through:db['groupconsumer']});

module.exports = db;
