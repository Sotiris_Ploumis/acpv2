module.exports = function(sequelize, Sequelize) {

    var GroupConsumer = sequelize.define('groupconsumer', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        owner: {
            type: Sequelize.INTEGER,
            notEmpty: true
        },
        active: {
            type: Sequelize.INTEGER,
            notEmpty: true
        },
        groupId:{
            type: Sequelize.INTEGER,
            notEmpty: true
        },
        consumerId:{
            type: Sequelize.INTEGER,
            notEmpty: true
        }
    });

    return GroupConsumer;

};