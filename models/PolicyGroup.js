module.exports = function(sequelize, Sequelize) {

    var PolicyGroup = sequelize.define('policygroup', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        owner: {
            type: Sequelize.INTEGER,
            notEmpty: true
        },
        active: {
            type: Sequelize.INTEGER,
            notEmpty: true
        },
        policyId:{
            type: Sequelize.INTEGER,
            notEmpty: true
        },
        groupId:{
            type: Sequelize.INTEGER,
            notEmpty: true
        }
    });

    return PolicyGroup;

};