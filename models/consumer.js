module.exports = function(sequelize, Sequelize) {

    var Consumer = sequelize.define('consumer', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        name: {
            type: Sequelize.STRING,
            notEmpty: true
        },
        owner: {
            type: Sequelize.INTEGER,
            notEmpty: true
        },
        active: {
            type: Sequelize.INTEGER,
            notEmpty: true
        },
        username: {
            type: Sequelize.STRING,
            notEmpty: true
        },
        password: {
            type: Sequelize.STRING,
            notEmpty: true
        }
    });

    return Consumer;

};