module.exports = function(sequelize, Sequelize) {

    var Group = sequelize.define('group', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        name: {
            type: Sequelize.STRING,
            notEmpty: true
        },
        owner: {
            type: Sequelize.INTEGER,
            notEmpty: true
        },
        active: {
            type: Sequelize.INTEGER,
            notEmpty: true
        }
    });

    return Group;

};