module.exports = function(sequelize, Sequelize) {

    var Policy = sequelize.define('policy', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        name: {
            type: Sequelize.STRING,
            notEmpty: true
        },
        owner: {
            type: Sequelize.INTEGER,
            notEmpty: true
        },
        description: {
            type: Sequelize.TEXT
        },
        active: {
            type: Sequelize.INTEGER,
            notEmpty: true
        },
        uri:{
            type: Sequelize.TEXT,
            notEmpty: true
        }
    });

    return Policy;

};