var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport   = require('passport');
var session    = require('express-session');
var env = require('dotenv').load();
var hbs = require('express-handlebars');
var helpers = require('hbs-passport-helpers');
//var io = require('socket.io');
console.log('app.js');

var app = express();

//for BodyParser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// For Passport
app.use(cookieParser());
app.use(session({ secret: 'sotosACP',resave: true, saveUninitialized:true})); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine( 'hbs', hbs( {
    extname: 'hbs',
    partialsDir: __dirname + '/views/partials/'
} ) );
app.set('view engine', '.hbs');
app.use(helpers());

//Models
app.set('models', require('./models'));


//Routes
var index = require('./routes/index');
var authRoute = require('./routes/auth.js')(app, passport);
var policyRoute = require('./routes/policy.js')(app);
var groupRoute = require('./routes/group.js')(app);
var consumerRoute = require('./routes/consumer')(app);
var relationRoute = require('./routes/relations')(app);

//load passport strategies
require('./config/passport/passport.js')(passport, app.get('models').user);

//Sync Database
app.get('models').sequelize.sync().then(function() {
    console.log('Nice! Database looks fine')
}).catch(function(err) {
    console.log(err, "Something went wrong with the Database Update!")
});

//done

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', index);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
