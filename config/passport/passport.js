var bCrypt = require('bcrypt-nodejs');
var date = require('date-and-time');

module.exports = function(passport, user) {
    var User = user;
    var LocalStrategy = require('passport-local').Strategy;

    //serialize
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // deserialize user
    passport.deserializeUser(function(id, done) {
        User.findById(id).then(function(user) {
            if (user) {
                done(null, user.get());
            } else {
                done(user.errors, null);
            }
        });
    });

    passport.use('local-signup', new LocalStrategy(
        {
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        function(req, email, password, done) {
            console.log('post signup strategy');
            var generateHash = function(password) {
                return bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);
            };
            var fname = req.body.firstname;
            var lname = req.body.lastname;
            var username = fname.substring(0,1) + lname;
            User.findOne({
                where: {
                    email: email
                }
            }).then(function(user) {
                if(user){
                    return done(null, false, {
                        message: 'That email is already taken'
                    });
                }else{
                    var userPassword = generateHash(password);
                    var timeNow = new Date();
                    date.format(timeNow, 'YYYY/MM/DD HH:mm:ss');
                    var data =
                        {
                            email: email,
                            password: userPassword,
                            firstname: fname,
                            lastname: lname,
                            username: username,
                            last_login: timeNow
                        };
                    User.create(data).then(function(newUser, created) {
                        if (!newUser) {
                            return done(null, false);
                        }
                        if (newUser) {
                            return done(null, newUser);
                        }
                    });
                }
            });
        }
    ));
    //LOCAL SIGNIN
    passport.use('local-signin', new LocalStrategy(
        {
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        function(req, email, password, done) {
            console.log('post signin');
            var User = user;
            var isValidPassword = function(userpass, password) {
                return bCrypt.compareSync(password, userpass);
            };
            var timeNow = new Date();
            //var validPassword = isValidPassword(user.password, password);
            date.format(timeNow, 'YYYY/MM/DD HH:mm:ss');
            User.findOne({
                where: {
                    email: email
                }
            }).then(function(user) {
                if (!user) {
                    return done(null, false, {
                        message: 'Email does not exist'
                    });
                }
                if (!isValidPassword(user.password, password)) {
                    return done(null, false, {
                        message: 'Incorrect password.'
                    });
                }

                var userinfo = user.get();
                return done(null, userinfo);
            }).catch(function(err) {
                console.log("Error:", err);
                return done(null, false, {
                    message: 'Something went wrong with your Signin'
                });
            });
            User.update(
                {last_login:timeNow},
                {where:{email:email}}
            ).spread(function () {
                console.log('last login updated with '+ timeNow);
            }).catch(function () {
                console.log('last login update failed');
            });

            console.log('done signin');
        }
    ));

};